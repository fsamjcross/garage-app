<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use App\Validators\BookingTimeValidator;
use Illuminate\Support\Facades\Validator;
use App\Validators\SlotAvailabilityValidator;
use Illuminate\Support\Facades\Config;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Validator::extend('allowed_booking_time', function ($attribute, $value, $parameters, $validator) {
            $time = Carbon::parse($value);
            $minute = $time->minute;
            return $minute === 0 || $minute === 30;
        });
        Validator::extend('available_day', function ($attribute, $value, $parameters, $validator) {
            $time = Carbon::parse($value);
            $openDays = explode(',', Config::get('app.open_days'));

            $isValidDay = in_array($time->englishDayOfWeek, $openDays);

            return $isValidDay;
        });
        Validator::extend('available_time', function ($attribute, $value, $parameters, $validator) {
            $time = Carbon::parse($value);
            $openHours = Config::get('app.open_hours');

            $start = Carbon::parse($openHours['start'])->setDate($time->year, $time->month, $time->day);
            $end = Carbon::parse($openHours['end'])->setDate($time->year, $time->month, $time->day);

            return $time->isBetween($start, $end, true);

        });
       Validator::extend('not_within_blocked_slot', function ($attribute, $value, $parameters, $validator) {
            $startTime = $value;
            $endTime = date('Y-m-d H:i:s', strtotime('+30 minutes', strtotime($value)));

            $blockedSlots = \App\Models\BlockedSlot::where(function ($query) use ($startTime, $endTime) {
                $query->where('datetime', '>=', $startTime)
                    ->where('datetime', '<', $endTime);
            })->exists();

            return !$blockedSlots;
        });
 
    }
}