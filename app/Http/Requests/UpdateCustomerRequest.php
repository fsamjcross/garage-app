<?php

namespace App\Http\Requests;

use App\Validators\SlotAvailabilityValidator;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCustomerRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:customers,email,' . $this->customer->id,
            'phone' => 'required',
            'vehicle_make_model' => 'required',
            'booking_datetime' => 'required|date|allowed_booking_time|available_time|available_day|not_within_blocked_slot'
        ];
    }

    public function messages()
    {
        return [
            'allowed_booking_time' => 'Booking time must be on the hour or on 30 minutes past the hour.',
            'available_time' => 'The selected time slot is not available.',
            'available_day' => 'The selected day is not available.',
            'not_within_blocked_slot' => 'The :attribute falls within a blocked slot.',

        ];
    }
}
