<?php
namespace App\Http\Controllers;

use App\Mail\AdminNotification;
use App\Http\Requests\CreateCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Models\Customer;
use App\Mail\CustomerCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class CustomersController extends Controller
{
    public function index()
    {
        $customers = Customer::all();

        return response()->json($customers);
    }


   public function store(CreateCustomerRequest $request)
    {
        $customer = Customer::create($request->validated());

        // Send email to customer
        Mail::to($customer->email)->send(new CustomerCreated($customer));

        // Send email to admin
        $adminEmail = Config::get('mail.admin_email');
        Mail::to($adminEmail)->send(new AdminNotification($customer));

        return response()->json($customer, 201);
    } 

    public function show(Customer $customer)
    {
        return response()->json($customer);
    }

    public function update(UpdateCustomerRequest $request, Customer $customer)
    {
        $customer->update($request->validated());

        return response()->json($customer);
    }

    public function destroy(Customer $customer)
    {
        $customer->delete();

        return response()->json(null, 204);
    }
}
