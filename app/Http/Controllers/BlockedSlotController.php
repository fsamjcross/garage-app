<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBlockedSlotRequest;
use App\Models\BlockedSlot;

class BlockedSlotController extends Controller
{
    public function index()
    {
        $blockedSlots = BlockedSlot::all();

        return response()->json($blockedSlots);
    }

    public function store(StoreBlockedSlotRequest $request)
    {
        $blockedSlot = BlockedSlot::create([
            'datetime' => $request->datetime
        ]);

        return response()->json($blockedSlot,201);
    }

    public function destroy($id)
    {
        $blockedSlot = BlockedSlot::findOrFail($id);
        $blockedSlot->delete();

        
        return response()->json(null, 204);

    }
}