import { createApp } from 'vue/dist/vue.esm-bundler.js';
import { createPinia } from 'pinia';
import { createRouter, createWebHistory } from 'vue-router';

import MakeOrder from './pages/makeOrder.vue'
import Admin from './pages/admin.vue'
import VueDatePicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css'


const app = createApp({})

app.component('VueDatePicker', VueDatePicker);

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/book', component: MakeOrder },
    { path: '/admin', component: Admin },
  ],
});

const pinia = createPinia();
app.use(pinia);

app.use(router);

router.isReady().then(() => {
  app.mount('#app');
});