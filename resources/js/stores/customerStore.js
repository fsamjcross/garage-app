import { defineStore } from 'pinia';
import axios from 'axios';

export const useCustomerStore = defineStore('customer', {
  state: () => ({
    customers: [],
    lastError: '',
  }),
  actions: {
    async fetchCustomers() {
      try {
        const response = await axios.get('/api/customers');
        this.customers = response.data;
      } catch (error) {
        this.lastError = error;
      }
    },
    async createCustomer(customerData) {
      try {
        const response = await axios.post('/api/customers', customerData);
        this.customers.push(response.data);
      } catch (error) {
        this.lastError = error.response.data.message;
      }
    },
    async updateCustomer(customerId, customerData) {
      try {
        const response = await axios.put(`/api/customers/${customerId}`, customerData);
        const index = this.customers.findIndex((customer) => customer.id === customerId);
        if (index !== -1) {
          this.customers[index] = response.data;
        }
      } catch (error) {
        this.lastError = error;
      }
    },
    async deleteCustomer(customerId) {
      try {
        await axios.delete(`/api/customers/${customerId}`);
        this.customers = this.customers.filter((customer) => customer.id !== customerId);
      } catch (error) {
        this.lastError = error;
      }
    },
  },
  getters: {
    getLastError() {
      return this.lastError;
    },
  },
});
