import { defineStore } from 'pinia';
import axios from 'axios';

export const useBlockedSlotsStore = defineStore('blockedSlots', {
  state: () => ({
    blockedSlots: [],
    lastError: null,
  }),
  actions: {
    async fetchBlockedSlots() {
      try {
        const response = await axios.get('/api/blocked-slots');
        this.blockedSlots = response.data;
        this.lastError = null;
      } catch (error) {
        console.error('Failed to fetch blocked slots:', error);
        this.lastError = error.message; 
      }
    },
    async createBlockedSlot(blockedSlotData) {
      try {
        const response = await axios.post('/api/blocked-slots', blockedSlotData);
        this.blockedSlots.push(response.data);
        this.lastError = null;
      } catch (error) {
        console.error('Failed to create blocked slot:', error);
        this.lastError = error.message;
      }
    },
    async deleteBlockedSlot(blockedSlotId) {
      try {
        await axios.delete(`/api/blocked-slots/${blockedSlotId}`);
        this.blockedSlots = this.blockedSlots.filter((slot) => slot.id !== blockedSlotId);
        this.lastError = null;
      } catch (error) {
        console.error('Failed to delete blocked slot:', error);
        this.lastError = error.message;
      }
    },
  },
});
