<!-- resources/views/emails/customer/created.blade.php -->
<!DOCTYPE html>
<html>
<head>
    <title>Welcome to our website</title>
</head>
<body>
    <h1>Welcome to our website, {{ $customer->name }}!</h1>
    <p>Thank you for joining our community.</p>
    <!-- Additional content for the email -->
</body>
</html>
