<!-- resources/views/emails/admin/notification.blade.php -->
<!DOCTYPE html>
<html>
<head>
    <title>New customer registration</title>
</head>
<body>
    <h1>New customer registration</h1>
    <p>A new customer has registered on our website:</p>
    <ul>
        <li>Name: {{ $customer->name }}</li>
        <li>Email: {{ $customer->email }}</li>
        <!-- Additional customer details -->
    </ul>
</body>
</html>
