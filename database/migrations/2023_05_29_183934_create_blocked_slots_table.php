<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlockedSlotsTable extends Migration
{
    public function up()
    {
        Schema::create('blocked_slots', function (Blueprint $table) {
            $table->id();
            $table->datetime('datetime');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('blocked_slots');
    }
}
